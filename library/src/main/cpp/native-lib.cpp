//JNI function / structure using doc here:
//  https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html



#include <jni.h>
#include <string>
#import "lcms2.h"


extern "C" JNIEXPORT jintArray JNICALL
Java_com_variable_littlecms_LittleCMSManager_intents(JNIEnv *env, jobject /*this*/, jstring jniProfilePath) {
    const char *profilePath = env->GetStringUTFChars(jniProfilePath, NULL);

    cmsHPROFILE cmykProfile = cmsOpenProfileFromFile(profilePath, "r");

    //make a fake int array just to test passing arrays back to java land:
    bool is0, is1, is2, is3;
    is0 = is1 = is2 = is3 = false;
    int total = 0;
    for (int i=0; i<4; i++){
        if (cmsIsIntentSupported(cmykProfile, i, LCMS_USED_AS_OUTPUT)){
            switch (i){
                case 0: is0 = true; total++; break;
                case 1: is1 = true; total++; break;
                case 2: is2 = true; total++; break;
                case 3: is3 = true; total++; break;
            }
        }
    }
    int *intents = new int[total];
    int idx = 0;
    if (is0) intents[idx++] = 0;
    if (is1) intents[idx++] = 1;
    if (is2) intents[idx++] = 2;
    if (is3) intents[idx++] = 3;
    jintArray jarray = env->NewIntArray(total);
    env->SetIntArrayRegion(jarray, 0, total, intents);
    delete [] intents;
    cmsCloseProfile(cmykProfile);
    return jarray;
}



extern "C" JNIEXPORT jfloatArray JNICALL
Java_com_variable_littlecms_LittleCMSManager_lab2cmyk(JNIEnv *env, jobject /*this*/, jstring jniProfilePath, jint intent, jfloatArray jniLab){
    const char *profilePath = env->GetStringUTFChars(jniProfilePath, NULL);
    cmsHPROFILE cmykProfile = cmsOpenProfileFromFile(profilePath, "r");

    jsize  len = env->GetArrayLength(jniLab);
    float lab[3];
    float cmyk[4];
    if (len != 3){
        //we have a problem...
        cmsCloseProfile(cmykProfile);
        return 0;
    }
    if (!cmsIsIntentSupported(cmykProfile, intent, LCMS_USED_AS_OUTPUT)){
        //we have a problem....
        cmsCloseProfile(cmykProfile);
        return 0;
    }

    cmsHPROFILE labProfile = cmsCreateLab4Profile(NULL /*for D50 standard*/);

    jfloat *jlab = env->GetFloatArrayElements(jniLab, 0);
    for (int i=0; i < len; i++){
        lab[i] = jlab[i];
    }

    cmsHTRANSFORM xform = cmsCreateTransform(labProfile, TYPE_Lab_FLT, cmykProfile, TYPE_CMYK_FLT, intent, 0);

    cmsDoTransform(xform, lab, cmyk, 1);
    cmsCloseProfile(cmykProfile);
    cmsCloseProfile(labProfile);
    cmsDeleteTransform(xform);

    //scale:
    for (int i=0; i<4; i++){
        cmyk[i] /= 100.0;
    }
    jfloatArray jcmyk = env->NewFloatArray(4);
    env->SetFloatArrayRegion(jcmyk, 0, 4, cmyk);

    return jcmyk;

}
