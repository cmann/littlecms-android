package com.variable.littlecms;

import java.io.File;
import java.util.Iterator;
import java.util.List;

public class IccProfile {
    private File file;

    public IccProfile(File fileObject) {
        file = fileObject;
    }

    public String filename() {
        //extract filename from file:
        String profileStr = file.getAbsolutePath();
        return profileStr.substring(profileStr.lastIndexOf("/") + 1);
    }

    public String path() {
        return file.getAbsolutePath();
    }

    public static IccProfile fromPath(String path) {
        return new IccProfile(new File(path));
    }


    public List<IccIntent> availableIntents() {
        return LittleCMSManager.getInstance().availableIntentsFor(this.file);
    }

    public String intentsString() {
        StringBuilder strbul = new StringBuilder();
        Iterator<IccIntent> iter = availableIntents().iterator();
        while (iter.hasNext()) {
            strbul.append(iter.next());
            if (iter.hasNext()) {
                strbul.append(",");
            }
        }
        strbul.toString();
        return strbul.toString();
    }

    public float[] lab2cmyk(float[] d50lab, IccIntent intent) {
        return LittleCMSManager.getInstance().lab2cmyk(file, intent, d50lab[0], d50lab[1], d50lab[2]);
    }
}
