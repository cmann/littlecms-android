package com.variable.littlecms;

public enum IccIntent {
    PERCEPTUAL(0),
    RELATIVE(1),
    SATURATION(2),
    ABSOLUTE(3);
    private final int value;
    IccIntent(final int newValue) {
        value = newValue;
    }
    public int getValue() { return value; }
}
