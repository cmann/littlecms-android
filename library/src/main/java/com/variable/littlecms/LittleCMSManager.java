package com.variable.littlecms;


import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


public class LittleCMSManager {
    //Manager Singleton:
    private static final LittleCMSManager manager = new LittleCMSManager();

    private LittleCMSManager() {
    }

    public static LittleCMSManager getInstance() {
        return manager;
    }


    //C/C++ Native Interface Code:
    static {
        System.loadLibrary("native-lib");
    }

    //A native method that is implemented by the 'native-lib' native library,
    //keyword native denotes that this method is implemented in another language.
    public native int[] intents(String jniProfilePath);

    public native float[] lab2cmyk(String jniProfilePath, int intent, float[] lab);


    public void loadAssets(@NonNull AssetManager manager, @NonNull File dir) throws IOException {
        String[] assets = manager.list("icc_profiles");
        if (assets == null) {
            return;
        }

        for (String asset : assets) {
            //do not copy non icc profiles
            if (!asset.endsWith(".icc")) {
                continue;
            }

            // do not recopy icc profiles
            File destFile = new File(dir, asset);
            if(destFile.exists() && destFile.length() > 0){
                Log.d("Variable-SDK", "ignoring transfer request of icc profile asset, already exists. " + asset);
                continue;
            }

            try(InputStream is = manager.open(String.format("icc_profiles/%s", asset))) {
                transfer(is,  destFile);
            }
        }
    }

    public void transfer(InputStream in, @NonNull File destFile) throws IOException {
        if (!destFile.getName().endsWith(".icc")) {
            return;
        }

        if(!destFile.exists()){
            destFile.createNewFile();
        }

        try(OutputStream out = new FileOutputStream(destFile)) {

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            Log.d("Variable-SDK", "loadAssets'd file: " + destFile.getAbsolutePath());

        } catch (IOException e) {
            Log.e("Variable-SDK", "Exception in LittleCms.copyFile() of " + destFile);
            Log.e("Variable-SDK", "Exception in copyFile() " + e.toString());

            throw e;
        }
    }

    @NonNull
    public List<IccProfile> iccProfiles(File dir) {
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("pathDir must be a directory.");
        }

        File[] files = dir.listFiles();

        ArrayList<IccProfile> items = new ArrayList<IccProfile>();
        if(files == null){
            return items;
        }


        for (File file : files) {
            if (file.toString().endsWith(".icc")) {
                Log.d("Variable-SDK", "Got ICC File: " + file);
                items.add(new IccProfile(file));
            }
        }
        return items;
    }


    public List<IccIntent> availableIntentsFor(File profile) {
        int[] intentInts = intents(profile.getAbsolutePath());

        ArrayList<IccIntent> intents = new ArrayList<>();
        for (int num : intentInts) {
            switch (num) {
                case 0:
                    intents.add(IccIntent.PERCEPTUAL);
                    break;
                case 1:
                    intents.add(IccIntent.RELATIVE);
                    break;
                case 2:
                    intents.add(IccIntent.SATURATION);
                    break;
                case 3:
                    intents.add(IccIntent.ABSOLUTE);
                    break;
            }
        }
        return intents;
    }

    public float[] lab2cmyk(File profile, IccIntent intent, float l, float a, float b) {
        return lab2cmyk(
                profile.getAbsolutePath(),
                intent.getValue(),
                new float[]{l, a, b}
        );
    }


}
